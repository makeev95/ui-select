(function (argument) {
	'use strict';

 	angular.module('app', ['ui.bootstrap', 'ui.bootstrap.datetimepicker', 'ui.select']).controller('mainController', ['$http', function($http) {

 		const vm = this;

 		


 		vm.isOpen = false;

 		vm.open = (e) => {
 			e.preventDefault();
 			e.stopPropagation();
 			vm.isOpen = true;
 		}





 		vm.countries = [];

 		vm.fetchCountries = ($select, $event) => {

 			if (!$event) {
 				vm.page = 1;
 				vm.countries = [];
 			} else {
 				$event.stopPropagation();
 				$event.preventDefault();
 				vm.page++;
 			}

 			vm.loading = true;

			$http({
	 			method: 'GET',
	 			url: 'http://localhost:8080/rest/dict/countries',
	 			params: {page: vm.page, name: $select.search },
	 			headers: {'x-auth-token' : 'eyJzZXNzaW9uSWQiOiJhY2YzZjYyNy0xMWNjLTQ3MmQtODNlYi1iOGM0ZmQ1Y2Y3NGMifQ==.WHC2RFQAJAew3otzrkOz5mhqwaeyTFXIo+iLBurBaYM='},
 			}).then(function(resp) {
 				vm.countries = vm.countries.concat(resp.data);
 			})['finally'](function() {
 				vm.loading = false;
 			});
 		};

 	}]);

})();